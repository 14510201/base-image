# Use an official Ubuntu base image
FROM ubuntu:latest

# Install build essentials and other required tools
RUN apt-get update && apt-get install -y \
    build-essential \
    gcc \
    g++ \
    libssl-dev \
    cmake \
    make \
    libboost-all-dev \
    nlohmann-json3-dev \
    pkg-config \
    curl \
    git \
    zip \
    libpcap-dev \
    libssl-dev

# Clone and build hiredis
RUN git clone https://github.com/redis/hiredis.git && \
    cd hiredis && \
    git checkout tags/v1.2.0 -b v1.2.0-branch && \
    make && \
    make install

# Clone and build redis++
RUN git clone https://github.com/sewenew/redis-plus-plus.git && \
    cd redis-plus-plus && \
    git checkout tags/1.3.11 -b v1.3.11-branch && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install

# Clone and build PcapPlusPlus
RUN git clone https://github.com/seladb/PcapPlusPlus.git && \
    cd PcapPlusPlus && \
    git checkout tags/v23.09 -b v23.09-branch && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install

RUN grep -r "PCAPPLUSPLUS_VERSION" /usr/local/include/pcapplusplus/

RUN git clone https://github.com/oatpp/oatpp.git && \
    cd oatpp && \
    git checkout tags/1.3.0 -b v1.3.0-branch && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install

RUN git clone https://github.com/oatpp/oatpp-swagger.git && \
    cd oatpp-swagger && \
    git checkout tags/1.3.0 -b v1.3.0-branch && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
	make install

RUN apt-get install libsqlite3-dev
RUN git clone https://github.com/oatpp/oatpp-sqlite.git && \
    cd oatpp-sqlite && \
    git checkout tags/1.3.0 -b v1.3.0-branch && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install
RUN apt-get install liblog4cpp5-dev -y

# Cleanup
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



